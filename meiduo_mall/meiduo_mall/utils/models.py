from django.db import models


class BaseModel(models.Model):
    create_time = models.DateField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateField(verbose_name='修改时间', auto_now=True)

    class Meta:
        # 表示此模型是一个抽象模型， 迁移建表时不会做迁移建表动作
        abstract = True
