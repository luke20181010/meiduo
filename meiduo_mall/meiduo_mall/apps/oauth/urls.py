from django.conf.urls import url

from . import views

urlpatterns = [
    # ^qq/authorization/?next=xxx, 用户点击QQ登入发送的请求url, next是跳转登入前的页面地址
    url(r'^qq/authorization/$', views.QQAuthURLView.as_view()),
    # /oauth/qq/user/?code=4815EBD34BE004A9922C2231C3128D1C, 回调处理
    url(r'^qq/user/$', views.QQAuthUserView.as_view()),

]