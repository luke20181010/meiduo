from django.db import models

from meiduo_mall.utils.models import BaseModel
from users.models import User


class QQAuthUser(BaseModel):
    openid = models.CharField(verbose_name='QQ用户唯一标识', db_index=True, max_length=64)
    user = models.ForeignKey(User, verbose_name='openid关联用户', on_delete=models.CASCADE, )

    class Meta:
        db_table = 'tb_qq_auth'
        verbose_name = 'QQ登入用户'
        verbose_name_plural = verbose_name
