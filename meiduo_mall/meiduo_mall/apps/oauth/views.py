from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from QQLoginTool.QQtool import OAuthQQ
from django.conf import settings  # 获取settings中的QQ信息
from rest_framework import status
import logging

from rest_framework_jwt.settings import api_settings

from .models import QQAuthUser
from .utils import generate_save_user_token
from .serializers import QQAuthUserSerializer

logger = logging.getLogger('django')


class QQAuthURLView(APIView):
    def get(self, request):
        # ^qq/authorization/?next=xxx
        next = request.query_params.get('next')
        if not next:
            next = '/'
        # 创建对象， 然后调用get_qq_url方法， 用户登入QQ
        oauth = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                        client_secret=settings.QQ_CLIENT_SECRET,
                        redirect_uri=settings.QQ_REDIRECT_URI,
                        state=next)
        login_url = oauth.get_qq_url()
        return Response({'login_url': login_url})


class QQAuthUserView(APIView):
    # /oauth/qq/user/?code=4815EBD34BE004A9922C2231C3128D1C
    def get(self, request):
        # 获取access_code
        code = request.query_params.get('code')
        if not code:
            return Response({'message': '缺少code'}, status=status.HTTP_400_BAD_REQUEST)
        oauth = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                        client_secret=settings.QQ_CLIENT_SECRET,
                        redirect_uri=settings.QQ_REDIRECT_URI,
                        state=next)
        try:
            access_token = oauth.get_access_token(code=code)
            # 获取openid
            openid = oauth.get_open_id(access_token=access_token)
        except Exception as error:
            logger.info(error)
            return Response({'message': 'QQ服务器异常'}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        try:
            # 取得openid， 去数据库查询是否已经存在该openid
            qqauth_model = QQAuthUser.objects.get(openid=openid)
        except QQAuthUser.DoesNotExist:
            # 调用函数， 用itsdangerous加密openid

            openid_sin = generate_save_user_token(openid)
            # 返回给前段暂存
            return Response({'access_token': openid_sin})
        else:
            # 如果openid已经绑定过美多商城中的用户(生成jwt token直接让它登录成功)
            # 手动生成token
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER  # 加载生成载荷函数
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER  # 加载生成token函数
            # 获取user对象
            user = qqauth_model.user
            payload = jwt_payload_handler(user)  # 生成载荷
            token = jwt_encode_handler(payload)  # 根据载荷生成token
            return Response({
                'token': token,
                'username': user.username,
                'user_id': user.id
            })

    # 前端 用户绑定账号的表单提交路由为this.host + '/oauth/qq/user/'， 所以可以在这个视图下写post请求
    def post(self, request):
        #  创建序列化器进行发序列化
        serializer = QQAuthUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()  # .save()返回user
        # 手动生成token
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER  # 加载生成载荷函数
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER  # 加载生成token函数
        # 获取user对象
        payload = jwt_payload_handler(user)  # 生成载荷
        token = jwt_encode_handler(payload)  # 根据载荷生成token
        return Response({
            'token': token,
            'username': user.username,
            'user_id': user.id
        })
