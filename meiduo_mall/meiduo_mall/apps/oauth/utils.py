from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from django.conf import settings
from itsdangerous import BadData


def generate_save_user_token(openid):
    # 对openid加密
    serializer = Serializer(secret_key=settings.SECRET_KEY, expires_in=600)
    data = {'openid': openid}
    access_token_bytes = serializer.dumps(data)
    # 将加密后的openid返回
    return access_token_bytes.decode()


def check_save_user_token(openid):
    '''
    :param openid: 加密后的openid
    :return:
    '''
    serializer = Serializer(secret_key=settings.SECRET_KEY, expires_in=600)
    try:  # 设置了过期时长， 可能取不到
        data = serializer.loads(openid)
    except BadData:
        return None
    else:
        # data 是一个字典
        return data.get('openid')