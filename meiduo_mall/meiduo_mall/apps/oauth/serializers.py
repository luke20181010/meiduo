from rest_framework import serializers
from django_redis import get_redis_connection

from .utils import check_save_user_token
from users.models import User
from .models import QQAuthUser


class QQAuthUserSerializer(serializers.Serializer):
    access_token = serializers.CharField(label='qq')
    mobile = serializers.RegexField(label='手机号', regex=r'^1[3-9]\d{9}$')
    password = serializers.CharField(label='密码', min_length=8, max_length=20)
    sms_code = serializers.CharField(label='短信验证码')

    def validate(self, attrs):
        access_token = attrs.get('access_token')
        openid = check_save_user_token(access_token)  # 解密， 获取openid, 可能过期取不到openid
        if not openid:
            raise serializers.ValidationError('openid无效')

        # !!! 将解密后的openid加入到attrs字典中
        attrs["access_token"] = openid  # 将解密后的openid覆盖原先的经过加密的openid，
        # 以便于存储在数据库和直接用原生openid和用户的做比对

        sms_code = attrs.get('sms_code')
        mobile = attrs.get('mobile')
        # 从redis获取短信验证码
        redis_conn = get_redis_connection('verify_codes')
        real_sms_code = redis_conn.get('sms_%s' % mobile)  # 取出来的是bytes类型， 注意解码
        if real_sms_code is None:
            raise serializers.ValidationError('短信验证码过期')
        if real_sms_code.decode() != sms_code:
            raise serializers.ValidationError('短信验证码错误')
        try:
            # 如果能通过手机号查出对应user， 用户可能已经注册美多，
            # 还需要通过user.password取出密码和填写的密码比较
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            pass
        else:
            # 表示此手机号是已注册过的用户
            if not user.check_password(attrs.get('password')):
                raise serializers.ValidationError('已存在用户,但密码不正确')
            else:
                attrs['user'] = user

        return attrs

    def create(self, validated_data):
        """把openid和user进行绑定"""
        user = validated_data.get('user')
        if not user:  # 如果用户是不存在的,那就新增一个用户
            user = User(
                username=validated_data.get('mobile'),
                password=validated_data.get('password'),
                mobile=validated_data.get('mobile')
            )
            user.set_password(validated_data.get('password'))  # 对密码进行加密
            user.save()

        # 让user和openid绑定
        QQAuthUser.objects.create(
            user=user,
            openid=validated_data.get('access_token')
        )

        return user
