from django.db import models
from django.contrib.auth.models import AbstractUser
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadData

from django.conf import settings


class User(AbstractUser):
    # 增加手机号
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')
    # 增加邮箱是否激活
    email_active = models.BooleanField(verbose_name='邮箱验证', default=False)

    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name

    def generate_verify_email_url(self):
        # itsdangerous生成加密url
        serializer = Serializer(settings.SECRET_KEY, 24 * 60 * 60)
        data = {'user_id': self.id, 'mobile': self.mobile}
        token = serializer.dumps(data).decode()
        return 'http://www.meiduo.site:8080/success_verify_email.html?token=%s' % token

    @staticmethod
    def check_verify_email_token(token):
        '''
        没有用到self
        :param token: 获取到激活链接中的token
        :return:user
        '''
        serializer = Serializer(settings.SECRET_KEY, 24 * 60 * 60)

        try:
            data = serializer.loads(token)  # 可能token过期了， 或者被篡改
        except BadData:
            return None
        else:
            try:
                # 解密token得到字典， 取出字典中的id和mobile尝试去User表中查到用户对象
                user = User.objects.get(id=data.get('user_id'), mobile=data.get('mobile'))

            except User.DoesNotExist:
                return None
            else:
                return user


