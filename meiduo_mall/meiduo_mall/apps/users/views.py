from django.shortcuts import render
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .serializer import UserSerializer, UserDetailSerializer, EmailSerializer
from users.models import User


class EmailsVerifyView(APIView):
    def get(self, request):
        # 获取邮箱验证邮件查询参数 token
        token = request.query_params.get('token')
        if not token:
            return Response({'message':'token不存在'})
        # token解密， 获取用户对象
        user = User.check_verify_email_token(token=token)
        if not user:
            return Response({'message':'token错误'})
        # 将用户邮箱验证状态变更为True
        user.email_active = True
        user.save()
        # 不需要返回数据给前端
        return Response({'message':'OK'})


class EmailView(UpdateAPIView):
    # 保存邮箱
    # 权限校验， 需要登录用户
    permission_classes = [IsAuthenticated]
    serializer_class = EmailSerializer


    def get_object(self):
        return self.request.user

# class UserDetailView(APIView):
#     # 设置权限， 未登陆用户无法访问用户中心页面
#     permission_classes = [IsAuthenticated]
#
#     # 获取用户中心基本信息， username, mobile, email, email_active
#     def get(self, request):
#         user = request.user
#         serializer = UserDetailSerializer(user)
#         return Response(serializer.data)


class UserDetailView(RetrieveAPIView):
    # 设置权限， 未登陆用户无法访问用户中心页面
    permission_classes = [IsAuthenticated]
    serializer_class = UserDetailSerializer

    # 另一种方式， 直接继承RetrieveAPIView， 重写RetrieveModelMixin的retrieve方法的get_object方法
    # 需要query_set 和 serializer_class， 但是这个页面没有传递用户pk，所以无法得到query_set
    # 学不会 学不会， ╮(╯▽╰)╭
    def get_object(self):
        return self.request.user


class UserView(CreateAPIView):
    # 用户注册
    serializer_class = UserSerializer


class UsernameCountView(APIView):
    # 验证用户是否已经存在
    def get(self, request, username):
        count = User.objects.filter(username=username).count()
        data = {
            'count': count,
            'username': username
        }
        return Response(data)


class MobileCountView(APIView):
    # 验证手机号是否已经存在
    def get(self, request, mobile):
        count = User.objects.filter(mobile=mobile).count()
        data = {
            'count': count,
            'username': mobile
        }
        return Response(data)
