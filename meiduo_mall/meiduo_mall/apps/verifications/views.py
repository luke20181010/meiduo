import logging
from random import randint

from django_redis import get_redis_connection
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status


from celery_tasks.sms import constants
from meiduo_mall.libs.yuntongxun.sms import CCP
from celery_tasks.sms.tasks import send_sms_code

# 创建日志输出对象
logger = logging.getLogger('django')


class SMSCodeView(APIView):
    def get(self, request, mobile):
        # 创建redis存储对象
        redis_conn = get_redis_connection('verify_codes')
        # 尝试取出手机号的标记
        flag = redis_conn.get('send_flag_%s' % mobile)
        if flag:
            return Response({'message': '不要频繁获取短信验证码'}, status=status.HTTP_400_BAD_REQUEST)
        # 生成6位验证码
        sms_code = '%06d' % randint(0, 999999)
        logger.info(sms_code)
        # 创建redis管道
        pl = redis_conn.pipeline()
        # 存储验证码到redis
        pl.setex('sms_%s' % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        # 保存已发送的标记, 一分钟内发送过， 标记为1
        pl.setex('send_flag_%s' % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)
        # 执行管道
        pl.execute()
        # 发送短信
        # 异步celery发送短信验证码
        send_sms_code.delay(mobile, sms_code)
        return Response({'message': 'OK'})
