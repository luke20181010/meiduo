from celery import Celery
# 创建celery客户端
celery_app = Celery('meiduo')
# 加载celery配置文件
celery_app.config_from_object('celery_tasks.config')
# 注册celery任务
celery_app.autodiscover_tasks(['celery_tasks.sms'])